﻿using FactoryGame.Core;
using FactoryGame.Factory;
using TMPro;
using UnityEngine;

public class UIThing : MonoBehaviour
{
    private Harvester _factory;
    [SerializeField] private TMP_Text inputText;
    [SerializeField] private TMP_Text outPutText;

    // Start is called before the first frame update
    private void Start()
    {
        if (FindObjectOfType<FactorySceneManager>().Factory.Factory is Harvester
            factory) _factory = factory;
    }

    // Update is called once per frame
    private void Update()
    {
        if (_factory != null)
        {
            inputText.text = _factory.HarvesterInput[0].Count.ToString();
            outPutText.text = _factory.Outputs[0].Count.ToString();
        }
    }
}