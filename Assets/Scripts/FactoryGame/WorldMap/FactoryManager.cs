﻿using FactoryGame.Core;
using UnityEngine;

namespace FactoryGame.WorldMap
{
    public class FactoryManager : MonoBehaviour
    {
        public Factory.Factory Factory { get; protected set; }


        public virtual void Initialize(uint inputs, uint outputs)
        {
            Factory = new Factory.FactoryBuilder()
                .WithInputs(inputs)
                .WithOutputs(outputs)
                .WithSize(1)
                .Build();

            FindObjectOfType<TimeManager>().tickElapsed += Factory.Tick;
        }
    }
}