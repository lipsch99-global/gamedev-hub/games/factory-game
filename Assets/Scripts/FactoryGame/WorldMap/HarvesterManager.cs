﻿using FactoryGame.Core;
using FactoryGame.Core.Items;
using FactoryGame.Factory;

namespace FactoryGame.WorldMap
{
    public class HarvesterManager : FactoryManager
    {
        private ItemType[] _harvestedItems;


        public Harvester Harvester
        {
            get => (Harvester) Factory;
            private set => Factory = value;
        }

        public void Initialize(uint outputs, ItemType[] itemsToBeHarvested)
        {
            _harvestedItems = itemsToBeHarvested;

            Harvester = new HarvesterBuilder()
                .WithOutputs(outputs)
                .WithHarvesterInputs((uint) itemsToBeHarvested.Length)
                .Build();

            for (var i = 0; i < itemsToBeHarvested.Length; i++)
                Harvester.HarvesterInput[i] =
                    new ItemHarvester(itemsToBeHarvested[i], 5);

            FindObjectOfType<TimeManager>().tickElapsed += Factory.Tick;
        }
    }
}