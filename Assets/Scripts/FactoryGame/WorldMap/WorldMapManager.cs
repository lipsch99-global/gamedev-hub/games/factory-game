﻿using FactoryGame.Core;
using UnityEngine;

namespace FactoryGame.WorldMap
{
    public class WorldMapManager : MonoBehaviour
    {
        private static Grid _grid;
        private Camera _mainCamera;

        public static Grid GetGrid()
        {
            if (_grid == null) _grid = FindObjectOfType<Grid>();

            return _grid;
        }

        private void Awake()
        {
            _mainCamera = Camera.main;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var worldPoint =
                    _mainCamera.ScreenToWorldPoint(Input.mousePosition);

                var hit = Physics2D.Raycast(worldPoint, Vector2.zero);
                if (hit.collider != null)
                {
                    var factoryManager =
                        hit.collider.gameObject.GetComponent<FactoryManager>();
                    if (factoryManager != null)
                        FindObjectOfType<SceneManager>()
                            .GoToFactoryScene(factoryManager);
                }
            }
        }
    }
}