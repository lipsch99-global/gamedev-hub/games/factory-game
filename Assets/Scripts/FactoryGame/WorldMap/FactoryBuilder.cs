﻿using FactoryGame.Core.Items;
using UnityEngine;

namespace FactoryGame.WorldMap
{
    public class FactoryBuilder : MonoBehaviour
    {
        [SerializeField] private FactoryManager factoryManagerPrefab;
        [SerializeField] private HarvesterManager harvesterManagerPrefab;
        [SerializeField] private uint inputs;
        [SerializeField] private ItemType[] itemsToHarvest;
        [SerializeField] private uint outputs;


        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                BuildFactory();
            else if (Input.GetKeyDown(KeyCode.Alpha2)) BuildHarvester();
        }

        private void BuildFactory()
        {
            var newFactory = Instantiate(factoryManagerPrefab, transform);
            newFactory.Initialize(inputs, outputs);
        }

        private void BuildHarvester()
        {
            var newHarvester = Instantiate(harvesterManagerPrefab, transform);
            newHarvester.Initialize(outputs, itemsToHarvest);
        }
    }
}