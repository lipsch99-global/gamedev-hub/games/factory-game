﻿using UnityEngine;

namespace FactoryGame.Factory
{
    [CreateAssetMenu(menuName = "FactoryGame/Items/ItemRecipe")]
    public class ItemRecipe : ScriptableObject
    {
        [SerializeField] private ItemCount[] inputs;
        [SerializeField] private ItemCount[] outputs;

        public ItemCount[] Inputs => inputs;
        public ItemCount[] Outputs => outputs;
    }
}