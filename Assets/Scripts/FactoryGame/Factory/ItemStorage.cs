﻿using FactoryGame.Core.Items;
using UnityEngine;

namespace FactoryGame.Factory
{
    public class ItemStorage : FactoryNode
    {
        protected ItemType ItemType;

        public ItemStorage() : base(null, null)
        {
        }

        public uint Count { get; protected set; }


        public void SetItemType(ItemType itemType)
        {
            ItemType = itemType;
        }

        public override void Tick()
        {
            Debug.Log("Item Storage Tick");
            if (NodesAfter.Count > 0)
            {
                Count--;
                if (NodesAfter[0] is ItemStorage storage) storage.AddItem(1);
            }
        }

        public void AddItem(uint amount)
        {
            Count += amount;
        }
    }

    public class ItemHarvester : ItemStorage
    {
        private readonly uint _inputRatePerTick;

        public ItemHarvester(ItemType itemType, uint inputRatePerTick)
        {
            _inputRatePerTick = inputRatePerTick;
            ItemType = itemType;
            OutputType = itemType;
        }

        public override void Tick()
        {
            Debug.Log("Item Harvester Tick");
            Count += _inputRatePerTick;
            base.Tick();
        }
    }
}