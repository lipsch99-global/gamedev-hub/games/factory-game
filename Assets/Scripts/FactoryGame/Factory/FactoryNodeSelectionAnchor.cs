﻿using UnityEngine;

namespace FactoryGame.Factory
{
    public class FactoryNodeSelectionAnchor : MonoBehaviour, IHideable
    {
        private Collider2D _collider;
        [SerializeField] private GameObject innerPart;
        [SerializeField] private GameObject visualObject;


        public FactoryNode Node { get; set; }


        public void Show()
        {
            visualObject.SetActive(true);
            _collider.enabled = true;
        }

        public void Hide()
        {
            visualObject.SetActive(false);
            _collider.enabled = false;
        }

        private void Awake()
        {
            _collider = GetComponent<Collider2D>();
        }

        private void Start()
        {
            Hide();
            SetFilled(false);
        }

        public void SetFilled(bool filled)
        {
            innerPart.SetActive(filled);
        }
    }

    public interface IHideable
    {
        void Show();
        void Hide();
    }
}