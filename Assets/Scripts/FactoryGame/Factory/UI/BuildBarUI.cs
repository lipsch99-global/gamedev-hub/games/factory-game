﻿using UnityEngine;

namespace FactoryGame.Factory.UI
{
    // ReSharper disable once InconsistentNaming
    public class BuildBarUI : MonoBehaviour
    {
        public void SetVisibility(bool visible)
        {
            gameObject.SetActive(visible);
        }
    }
}