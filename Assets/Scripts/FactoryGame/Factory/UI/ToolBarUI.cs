﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace FactoryGame.Factory.UI
{
    // ReSharper disable once InconsistentNaming
    public class ToolBarUI : MonoBehaviour
    {
        private List<IHideable> _toHide;
        [SerializeField] private BuildBarUI buildBar;


        private void Start()
        {
            OnSelectBuildTool();
        }

        public void OnSelectLinkTool()
        {
            buildBar.SetVisibility(false);
            foreach (var hideable in _toHide) hideable.Show();
        }

        public void OnSelectBuildTool()
        {
            buildBar.SetVisibility(true);
            _toHide = FindObjectsOfType<FactoryNodeSelectionAnchor>()
                .Select(x => (IHideable) x).ToList();
            _toHide.AddRange(
                FindObjectsOfType<FactoryNodeLine>()
                    .Select(x => (IHideable) x));
            foreach (var hideable in _toHide) hideable.Hide();
        }
    }
}