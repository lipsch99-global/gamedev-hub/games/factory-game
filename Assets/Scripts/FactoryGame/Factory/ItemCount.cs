﻿using System;
using FactoryGame.Core.Items;
using UnityEngine;

namespace FactoryGame.Factory
{
    [Serializable]
    public class ItemCount
    {
        [SerializeField] private uint count;
        [SerializeField] private ItemType type;

        public uint Count
        {
            get => count;
            set => count = value;
        }
        public ItemType Type => type;


        public ItemCount(ItemType type)
        {
            this.type = type;
        }
        
    }
}
