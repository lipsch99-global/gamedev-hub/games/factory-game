﻿using UnityEngine;

namespace FactoryGame.Factory
{
    public class FactoryNodeLine : MonoBehaviour, IHideable
    {
//    [SerializeField] private FactoryNodeSelectionAnchor a;
//    [SerializeField] private FactoryNodeSelectionAnchor b;
        [SerializeField] private SpriteRenderer spriteRenderer;

        public void Show()
        {
            spriteRenderer.gameObject.SetActive(true);
        }

        public void Hide()
        {
            spriteRenderer.gameObject.SetActive(false);
        }


        private void Start()
        {
            Hide();
        }

        public void Align(FactoryNodeSelectionAnchor a,
            FactoryNodeSelectionAnchor b)
        {
            var aPos = a.transform.position;
            var bPos = b.transform.position;
            Align(aPos, bPos);
        }

        public void Align(Vector3 a, Vector3 b)
        {
            spriteRenderer.gameObject.SetActive(true);

            transform.position =
                Vector3.Lerp(a, b, 0.5f);

            var vectorToTarget = a - transform.position;
            var angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) *
                        Mathf.Rad2Deg;
            transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);

            spriteRenderer.size =
                new Vector2(
                    Vector3.Magnitude(a - b), 1);
        }
    }
}