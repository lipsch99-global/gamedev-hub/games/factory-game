﻿using UnityEngine;

namespace FactoryGame.Factory
{
    public class FactoryManager : MonoBehaviour
    {
        [SerializeField] private FactoryNodeSelectionAnchor[] inputs;
        [SerializeField] private FactoryNodeSelectionAnchor[] outputs;

        public FactoryNodeSelectionAnchor[] Inputs => inputs;
        public FactoryNodeSelectionAnchor[] Outputs => outputs;
    }
}