﻿namespace FactoryGame.Factory
{
    public class FactoryBuilder
    {
        protected uint Inputs;
        protected uint Outputs;
        protected uint Size;


        public FactoryBuilder WithInputs(uint count)
        {
            Inputs = count;
            return this;
        }

        public FactoryBuilder WithOutputs(uint count)
        {
            Outputs = count;
            return this;
        }

        public FactoryBuilder WithSize(uint size)
        {
            Size = size;
            return this;
        }

        public virtual Factory Build()
        {
            return new Factory(Inputs, Outputs, Size);
        }
    }

    public class HarvesterBuilder : FactoryBuilder
    {
        private uint _harvesterInputs;

        public HarvesterBuilder WithHarvesterInputs(uint count)
        {
            _harvesterInputs = count;
            return this;
        }

        public new HarvesterBuilder WithOutputs(uint count)
        {
            Outputs = count;
            return this;
        }


        public new Harvester Build()
        {
            return new Harvester(Outputs, _harvesterInputs, Size);
        }
    }
}