﻿using System.Collections.Generic;

namespace FactoryGame.Factory
{
    public class Harvester : Factory
    {
        public Harvester(uint outputAmount, uint harvesterInputAmount,
            uint size) :
            base(0, outputAmount, size)
        {
            HarvesterInput = new ItemHarvester[harvesterInputAmount];
        }

        public ItemHarvester[] HarvesterInput { get; set; }

        protected override List<FactoryNode> GetAllNodes()
        {
            var allNodes = new List<FactoryNode>();
            allNodes.AddRange(HarvesterInput);
            allNodes.AddRange(Outputs);

            return allNodes;
        }
    }
}