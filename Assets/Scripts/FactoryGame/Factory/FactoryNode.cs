﻿using System.Collections.Generic;
using FactoryGame.Core.Items;

namespace FactoryGame.Factory
{
    public class FactoryNode : ITickable
    {
        public FactoryNode(ItemType inputType, ItemType outputType)
        {
            InputType = inputType;
            OutputType = outputType;
            NodesAfter = new List<FactoryNode>();
            NodesBefore = new List<FactoryNode>();
        }

        public List<FactoryNode> NodesBefore { get; }
        public List<FactoryNode> NodesAfter { get; }

        public ItemType InputType { get; protected set; }
        public ItemType OutputType { get; protected set; }

        public virtual void Tick()
        {
        }

        public void AddNodeAfter(FactoryNode node)
        {
            NodesAfter.Add(node);
            node.NodesBefore.Add(this);
        }

        public void RemoveNodeAfter(FactoryNode node)
        {
            NodesAfter.Remove(node);
            node.NodesBefore.Remove(this);
        }

        public void AddNodeBefore(FactoryNode node)
        {
            NodesBefore.Add(node);
            node.NodesAfter.Add(this);
        }

        public void RemoveNodeBefore(FactoryNode node)
        {
            NodesBefore.Remove(node);
            node.NodesAfter.Remove(this);
        }

        public virtual bool CanBeConnectedAfter(FactoryNode node)
        {
            return node.OutputType == InputType || node.OutputType == null ||
                   this.InputType == null;
        }

        public virtual bool CanBeConnectedBefore(FactoryNode node)
        {
            return node.InputType == OutputType || node.InputType == null ||
                   this.OutputType == null;
        }
    }
}