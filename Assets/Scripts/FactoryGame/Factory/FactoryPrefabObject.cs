﻿using UnityEngine;

// ReSharper disable ConvertToAutoProperty

namespace FactoryGame.Factory
{
    [CreateAssetMenu(menuName = "FactoryGame/Factory/FactoryPrefab")]
    public class FactoryPrefabObject : ScriptableObject
    {
        public enum FactoryType
        {
            Factory,
            Harvester
        }

        [SerializeField] private GameObject factoryPrefab;
        [SerializeField] private int factorySize;
        [SerializeField] private FactoryType factoryType;

        [SerializeField] private int inputCount;
        [SerializeField] private int outputCount;

        public FactoryType TypeOfFactory => factoryType;
        public int InputCount => inputCount;
        public int OutputCount => outputCount;
        public int FactorySize => factorySize;
        public GameObject FactoryPrefab => factoryPrefab;
    }
}