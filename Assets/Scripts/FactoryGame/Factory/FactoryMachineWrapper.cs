﻿using System;
using System.Linq;
using FactoryGame.Core.Items;
using UnityEngine;

namespace FactoryGame.Factory
{
    public class FactoryMachineWrapper : MonoBehaviour
    {
        [SerializeField] private FactoryNodeSelectionAnchor[] inputs;
        [SerializeField] private FactoryNodeSelectionAnchor[] outputs;

        [SerializeField] private ItemRecipe recipe;
        public FactoryMachine FactoryMachine { get; private set; }

        private void Start()
        {
            FactoryMachine = new FactoryMachine(recipe);
            foreach (var t in inputs)
                t.Node = FactoryMachine;

            foreach (var t in outputs)
                t.Node = FactoryMachine;
        }
    }

    public class FactoryMachine : FactoryNode
    {
        public readonly ItemRecipe Recipe;

        public readonly FactoryNode[] InputNodes;
        public readonly FactoryNode[] OutputNodes;

        public FactoryMachine(ItemRecipe recipe) : base(
            null, null)
        {
            Recipe = recipe;

            InputNodes = new FactoryNode[Recipe.Inputs.Length];
            for (var i = 0; i < Recipe.Inputs.Length; i++)
            {
                InputNodes[i] = new FactoryNode(Recipe.Inputs[i].Type, null);
            }

            OutputNodes = new FactoryNode[Recipe.Outputs.Length];
            for (var i = 0; i < Recipe.Outputs.Length; i++)
            {
                OutputNodes[i] = new FactoryNode(null, Recipe.Outputs[i].Type);
            }
        }

        public override void Tick()
        {
            
        }

        public override bool CanBeConnectedAfter(FactoryNode node)
        {
            if (InputNodes.Any(x => x.InputType == node.OutputType))
            {
                return true;
            }
            else
            {
                if (node.OutputType == null)
                {
                    return true;
                }
            }

            return false;
        }

        public override bool CanBeConnectedBefore(FactoryNode node)
        {
            if (OutputNodes.Any(x => x.OutputType == node.InputType))
            {
                return true;
            }
            else
            {
                if (node.InputType == null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}