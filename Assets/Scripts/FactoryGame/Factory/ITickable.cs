﻿namespace FactoryGame.Factory
{
    public interface ITickable
    {
        void Tick();
    }
}