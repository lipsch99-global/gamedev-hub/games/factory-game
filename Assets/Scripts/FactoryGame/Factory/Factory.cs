﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FactoryGame.Factory
{
    public class Factory : ITickable
    {
        private readonly FactoryMachine[,] _factoryMachines;

        public Factory(uint inputAmount, uint outputAmount, uint size)
        {
            Nodes = new HashSet<FactoryNode>();
            Inputs = new ItemStorage[inputAmount];
            for (var i = 0; i < Inputs.Length; i++)
                Inputs[i] = new ItemStorage();

            Outputs = new ItemStorage[outputAmount];
            for (var i = 0; i < Outputs.Length; i++)
                Outputs[i] = new ItemStorage();

            _factoryMachines = new FactoryMachine[size * 5, size * 5];
        }

        public ItemStorage[] Inputs { get; set; }
        public ItemStorage[] Outputs { get; set; }

        public HashSet<FactoryNode> Nodes { get; protected set; }

        public virtual void Tick()
        {
            Debug.Log("Factory Tick");

            var tickedNodes = new HashSet<FactoryNode>();
            var nodesForIteration = new HashSet<FactoryNode>(tickedNodes);
            foreach (var output in Outputs) nodesForIteration.Add(output);


            do
            {
                foreach (var node in nodesForIteration)
                {
                    node.Tick();
                    tickedNodes.Add(node);
                }

                nodesForIteration = GetNodesForIteration(nodesForIteration);
            } while (nodesForIteration.Count > 0);


            var allNodes = GetAllNodes();

            allNodes.RemoveAll(x => tickedNodes.Contains(x));
            foreach (var node in allNodes) node.Tick();
        }

        public void CalculateOutputs()
        {
            throw new NotImplementedException();
        }

        public void AddFactoryMachine(Vector2Int position,
            FactoryMachine factoryMachine)
        {
            _factoryMachines[position.x, position.y] = factoryMachine;
        }

        public FactoryMachine RemoveFactoryMachine(Vector2Int position)
        {
            var removedMachine = _factoryMachines[position.x, position.y];
            _factoryMachines[position.x, position.y] = null;
            return removedMachine;
        }

        private HashSet<FactoryNode> GetNodesForIteration(
            HashSet<FactoryNode> lastTicked)
        {
            var returnValue = new HashSet<FactoryNode>();
            foreach (var node in lastTicked)
            foreach (var nodeBefore in node.NodesBefore)
                returnValue.Add(nodeBefore);

            return returnValue;
        }


        protected virtual List<FactoryNode> GetAllNodes()
        {
            var allNodes = new List<FactoryNode>();
            allNodes.AddRange(Inputs);
            allNodes.AddRange(Outputs);

            return allNodes;
        }
    }
}