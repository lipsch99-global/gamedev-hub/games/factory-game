﻿using UnityEngine;

namespace FactoryGame
{
    public class CharacterController : MonoBehaviour
    {
        private static readonly int X = Animator.StringToHash("X");
        private static readonly int Y = Animator.StringToHash("Y");
        private static readonly int Running = Animator.StringToHash("Running");
        [SerializeField] private Animator animator;
        [SerializeField] private float speed;

        private void Update()
        {
            var input = new Vector2(Input.GetAxis("Horizontal"),
                Input.GetAxis("Vertical"));

            animator.SetFloat(X, input.x);
            animator.SetFloat(Y, input.y);

            animator.SetBool(Running, input.magnitude > 0.1);

            transform.position = transform.position +
                                 Time.deltaTime * speed * (Vector3) input;
        }
    }
}