﻿using System;
using System.Linq;
using FactoryGame.Factory;
using FactoryGame.WorldMap;
using UnityEngine;
using FactoryManager = FactoryGame.WorldMap.FactoryManager;

namespace FactoryGame.Core
{
    public class FactorySceneManager : MonoBehaviour
    {
        private FactoryNodeSelectionAnchor _from;

        private Camera _mainCamera;

        private FactoryNodeLine _nodeLine;
        private FactoryNodeSelectionAnchor _to;
        [SerializeField] private FactoryMachineWrapper buildingPrefab;
        [SerializeField] private FactoryPrefabObject[] factoryPrefabs;
        [SerializeField] private FactoryNodeLine linePrefab;

        [SerializeField] private Grid grid;
        public Grid TheGrid => grid;

        public FactoryManager Factory { get; private set; }

        public void Start()
        {
            _mainCamera = Camera.main;
        }

        // Update is called once per frame
        private void Update()
        {
            HandleInputForFactoryNodeLinking();
            HandleBuilding();
        }

        private void HandleBuilding()
        {
            if (Input.GetKey(KeyCode.LeftShift) && Input.GetMouseButtonDown(0))
            {
                var roughWorldPoint =
                    _mainCamera.ScreenToWorldPoint(Input.mousePosition);
                var gridPoint = TheGrid.WorldToCell(roughWorldPoint);
                PlaceMachine(gridPoint, buildingPrefab);
            }
        }

        private void HandleInputForFactoryNodeLinking()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var worldPoint =
                    _mainCamera.ScreenToWorldPoint(Input.mousePosition);

                var hit = Physics2D.Raycast(worldPoint, Vector2.zero);
                if (hit.collider != null)
                {
                    var selectionAnchor = hit.collider.gameObject
                        .GetComponent<FactoryNodeSelectionAnchor>();
                    if (selectionAnchor != null)
                    {
                        if (_from == null)
                        {
                            _from = selectionAnchor;
                            selectionAnchor.SetFilled(true);
                            _nodeLine = Instantiate(linePrefab);
                        }
                        else
                        {
                            _to = selectionAnchor;
                            selectionAnchor.SetFilled(true);
                            _nodeLine.Align(_from, _to);
                            AddNodeConnection(_from.Node, _to.Node);
                            _to = null;
                            _from = null;
                        }
                    }
                }
            }
            else
            {
                if (_from != null && _to == null)
                {
                    var mousePos =
                        _mainCamera.ScreenToWorldPoint(Input.mousePosition);
                    _nodeLine.Align(_from.transform.position, mousePos);
                }
            }

//            if (Input.GetKeyDown(KeyCode.Escape))
//            {
//                _from.SetFilled(false);
//                _to.SetFilled(false);
//                RemoveNodeConnection(_from.Node, _to.Node);
//                _from = null;
//                _to = null;
//                _nodeLine.Hide();
//                Destroy(_nodeLine.gameObject);
//                _nodeLine = null;
//            }
        }

        public void Initialize(FactoryManager factoryManager)
        {
            Factory = factoryManager;

            var inputCount = factoryManager.Factory.Inputs.Length;
            var outputCount = factoryManager.Factory.Outputs.Length;
            var factoryType = FactoryPrefabObject.FactoryType.Factory;

            if (factoryManager is HarvesterManager)
            {
                var harvesterManager = (HarvesterManager) factoryManager;
                inputCount = harvesterManager.Harvester.HarvesterInput.Length;
                factoryType = FactoryPrefabObject.FactoryType.Harvester;
            }

            var correctPrefab = factoryPrefabs.FirstOrDefault(x =>
                    x.InputCount == inputCount &&
                    x.OutputCount == outputCount &&
                    x.TypeOfFactory == factoryType
                )
                ?.FactoryPrefab;

            if (correctPrefab == null)
                throw new Exception(
                    $"Factory Prefab was not found for {inputCount}_{outputCount}");

            var newFactory = Instantiate(correctPrefab, TheGrid.transform);


            LinkNodeSelectionAnchorsToNodes(newFactory
                .GetComponent<Factory.FactoryManager>());
        }

        private void AddNodeConnection(FactoryNode from, FactoryNode to)
        {
            from.AddNodeAfter(to);
            if (to is ItemStorage itemStorage)
                itemStorage.SetItemType(from.OutputType);
        }

        private void RemoveNodeConnection(FactoryNode from, FactoryNode to)
        {
            from.RemoveNodeAfter(to);
        }

        private void LinkNodeSelectionAnchorsToNodes(
            Factory.FactoryManager factoryManager)
        {
            if (Factory.Factory is Harvester harvester)
                for (var i = 0; i < harvester.HarvesterInput.Length; i++)
                    factoryManager.Inputs[i].Node = harvester.HarvesterInput[i];
            else
                for (var i = 0; i < Factory.Factory.Inputs.Length; i++)
                    factoryManager.Inputs[i].Node = Factory.Factory.Inputs[i];

            for (var i = 0; i < Factory.Factory.Outputs.Length; i++)
                factoryManager.Outputs[i].Node = Factory.Factory.Outputs[i];
        }

        public void PlaceMachine(Vector3Int position,
            FactoryMachineWrapper prefab)
        {
            Instantiate(prefab, TheGrid.CellToWorld(position),
                Quaternion.identity);
        }
    }
}