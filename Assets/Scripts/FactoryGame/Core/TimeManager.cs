﻿using System;
using FactoryGame.Factory;
using UnityEngine;

namespace FactoryGame.Core
{
    public class TimeManager : MonoBehaviour
    {
        [SerializeField] private float _currentSpeed = 1;

        private float _currentTickLength;

        private FactoryManager[] _factoryManagers;
        public Action tickElapsed;

        [SerializeField] private float tickLength;


        private void Update()
        {
            _currentTickLength += Time.deltaTime * _currentSpeed;
            if (_currentTickLength >= tickLength)
            {
                _currentTickLength = 0;
                tickElapsed?.Invoke();
            }
        }

        public void SetSpeed(float speedMultiplier)
        {
            _currentSpeed = Mathf.Clamp(speedMultiplier, 0, float.MaxValue);
        }

        private void OnValidate()
        {
            _currentSpeed = Mathf.Clamp(_currentSpeed, 0, float.MaxValue);
        }
    }
}