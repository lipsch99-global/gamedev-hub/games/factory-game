﻿using UnityEngine;

namespace FactoryGame.Core.Items
{
    [CreateAssetMenu(menuName = "FactoryGame/Items/ItemType")]
    public class ItemType : ScriptableObject
    {
        [SerializeField] private string name;
        [SerializeField] private Sprite icon;

        public string Name => name;
        public Sprite Icon => icon;
    }
}