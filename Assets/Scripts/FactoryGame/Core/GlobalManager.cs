﻿using UnityEngine;

namespace FactoryGame.Core
{
    public class GlobalManager : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start()
        {
            DontDestroyOnLoad(gameObject);
        }
    }
}