﻿using FactoryGame.WorldMap;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FactoryGame.Core
{
    public class SceneManager : MonoBehaviour
    {
        private FactoryManager _factoryOpened;
        private Scene _worldMap;
        [SerializeField] private GameObject worldmapCamera;
        [SerializeField] private GameObject worldmapUi;

        // Start is called before the first frame update
        private void Start()
        {
            _worldMap =
                UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            UnityEngine.SceneManagement.SceneManager.sceneLoaded +=
                SceneManagerOnSceneLoaded;
            UnityEngine.SceneManagement.SceneManager.sceneUnloaded +=
                SceneManagerOnSceneUnloaded;
        }

        private void SceneManagerOnSceneUnloaded(Scene scene)
        {
            UnityEngine.SceneManagement.SceneManager.SetActiveScene(_worldMap);
            worldmapCamera.SetActive(true);
            worldmapUi.SetActive(true);
        }


        private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode mode)
        {
            UnityEngine.SceneManagement.SceneManager.SetActiveScene(scene);
            worldmapCamera.SetActive(false);
            worldmapUi.SetActive(false);
            if (scene.buildIndex == 1)
                FindObjectOfType<FactorySceneManager>()
                    .Initialize(_factoryOpened);
        }

        [ContextMenu("Factory")]
        public void GoToFactoryScene(FactoryManager factoryManager)
        {
            var activeScene =
                UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            if (activeScene.buildIndex == 1) return;

            _factoryOpened = factoryManager;

            UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(1,
                LoadSceneMode.Additive);
        }


        [ContextMenu("Map")]
        public void GoToWorldMap()
        {
            var activeScene =
                UnityEngine.SceneManagement.SceneManager.GetActiveScene();
            if (activeScene.buildIndex == 0) return;

            _factoryOpened = null;

            UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(1);
        }
    }
}